#![feature(exit_status_error)]
#![feature(iter_advance_by)]
extern crate native_tls;
use std::{io::{Write, Read}, env, collections::HashMap, process::{Command, Stdio}};
use chrono::{DateTime, Duration};
use json::JsonValue;
use mailparse::{MailHeaderMap, DispositionType::Attachment};
use fancy_regex::Regex;

const FOLDER_BLACKLIST: [&str; 4] = ["Trash", "Sent", "Drafts", "All Mail"];

#[derive(Debug)]
enum IMAPSecurity
{
	SSLTLS,
	STARTTLS
}

impl core::fmt::Display for IMAPSecurity
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result
	{
		std::fmt::Debug::fmt(self, f)
	}
}

#[derive(Debug)]
enum IMAPAuthentication
{
	PENDING,
	PASSWORD(String),
}

impl core::fmt::Display for IMAPAuthentication
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result
	{
		std::fmt::Debug::fmt(self, f)
	}
}

struct IMAPConfig
{
	server: String,
	port: u16,
	username: String,
	security: IMAPSecurity,
	auth: IMAPAuthentication,
	cert: Option<String>,
}

impl core::fmt::Display for IMAPConfig
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result
	{
		let server = &self.server;
		let port = &self.port;
		let username = &self.username;
		write!(f, "{username}({server}:{port})")
	}
}

static mut VERBOSE: bool = false;

macro_rules! log
{
	($important:expr, $($tts:tt)*) =>
	{
		unsafe
		{
			if $important || VERBOSE
			{
				println!($($tts)*);
			}
		}
	}
}

type BoxErr = Box<dyn std::error::Error>;

fn get_string_value_with_interpolation(json: &JsonValue, injectables: &HashMap<String, String>) -> Option<String>
{
	json.as_str().and_then(|json|
	{
		let mut result = injectables.iter().fold(json.to_string(), |current, (injectable_name, injectable_value)|
		{
			match Regex::new(format!("{}{}{}", r"\{", injectable_name, r"\}").as_str())
			{
				Ok(regex) =>
				{
					regex.replace_all(&current, injectable_value).to_string()
				},
				_ => current,
			}
		});
		for (key, val) in env::vars()
		{
			Regex::new(format!("{}{}{}", r"\{", key, r"\}").as_str()).and_then(|regex|
			{
				result = regex.replace_all(result.as_str(), val.as_str()).to_string();
				Ok(())
			}).ok();
		}
		Some(result)
	})
}

fn prompt_password(source_name: String) -> String
{
	let mut pass = String::new();
	print!("Please enter password for {source_name}: ");
	std::io::stdout().flush().expect("Error flushing output");
	std::io::stdin().read_line(&mut pass).expect("Error prompting user for password");
	pass.as_str().trim().to_string()
}

fn parse_auth(source_name: String, mut auth: JsonValue) -> IMAPAuthentication
{
	let auth = auth.take_string().expect("auth must be a string");
	match auth.as_str()
	{
		"promptme" => IMAPAuthentication::PASSWORD(prompt_password(source_name)),
		val => IMAPAuthentication::PASSWORD(String::from(val)),
	}
}

struct ProcessMessageRequest<'a>
{
	source: &'a IMAPConfig,
	message: &'a imap::types::Fetch,
	triggers: &'a Vec<&'a JsonValue>,
	tmp_dir: &'a str,
	dry_run: bool,
	mailbox: &'a str,
}

fn usage_panic()
{
	panic!("Usage: neon-secretary [-vn] config.json");
}

fn main()
{
	let mut dry_run = false;
	let mut args: Vec<String> = env::args().collect();
	args.remove(0);
	let args: Vec<_> = args.iter().filter(|arg|
	{
		if arg.starts_with("-")
		{
			let mut iter = arg.chars();
			iter.advance_by(1).expect(format!("Could not iterate CLI arg: {}", arg).as_str());
			for c in iter
			{
				match c
				{
					'v' => unsafe { VERBOSE = true; },
					'n' => dry_run = true,
					_ => usage_panic(),
				}
			}
			return false;
		}
		return true;
	}).collect();
	if args.len() != 1 { usage_panic(); }
	if dry_run {log!(true, "Initiating dry run.");}
	let config_file = &args[0];
	let mut sources = Vec::<IMAPConfig>::new();
	let config = std::fs::read_to_string(config_file).expect(format!("Could not find {config_file}").as_str());
	let mut config = json::parse(config.as_str()).expect(format!("{config_file} was not valid JSON").as_str());
	assert!(config.is_object(), "{config_file} was not a JSON object");
	let mut sources_config = config["sources"].take();
	assert!(sources_config.is_array() && sources_config.len() > 0, "Sources must be non-empty array");
	for source in sources_config.members_mut()
	{
		let mut auth = source["auth"].take();
		let mut source = IMAPConfig
		{
			server: source["server"].take_string().expect("server must be a string"),
			port: source["port"].take().as_u16().unwrap_or(993),
			username: source["username"].take_string().expect("username must be a string"),
			security: match source["security"].take_string().unwrap_or(String::from("SSLTLS")).as_str()
			{
				"SSLTLS" => IMAPSecurity::SSLTLS,
				"STARTTLS" => IMAPSecurity::STARTTLS,
				sec => panic!("Unknown security type {sec}"),
			},
			auth: IMAPAuthentication::PENDING,
			cert: source["cert"].take_string(),
		};
		source.auth = parse_auth(source.to_string(), auth.take());
		log!(false, "Adding source: {source}");
		sources.push(source);
	}
	let triggers = &config["triggers"];
	assert!(triggers.is_array(), "triggers property must be an array");
	let triggers: Vec<&JsonValue> = triggers.members().collect();
	let cwd = env::current_dir().expect("Program had no CWD:");
	let cwd = cwd.to_str().ok_or("Program CWD was invalid:").expect("Could not select CWD:");
	let tmp_dir = format!("{}/neon-secretary.{}", env::var("TMPDIR").or::<()>(Ok(cwd.to_string())).unwrap(), std::process::id());
	std::fs::create_dir_all(&tmp_dir).expect(format!("Could not create temp directory: {}", &tmp_dir).as_str());
	for source in sources
	{
		log!(false, "Processing {source}");
		let mut tls = native_tls::TlsConnector::builder();
		match &source.cert
		{
			None => (),
			Some(path) =>
			{
				let pem_contents = std::fs::read_to_string(path).expect(format!("Could not read cer: {path}").as_str());
				tls.add_root_certificate(native_tls::Certificate::from_pem(pem_contents.as_bytes()).expect(format!("Not a valid PEM file: {path}").as_str()));
				log!(false, "Added cert: {path}");
			},
		}
		let tls = tls.build().expect("Unable to create TlsConnector");
		let domain = &source.server;
		let port = source.port;
		let addr = (domain.to_string(), port);
		let security = &source.security;
		log!(false, "Connecting to {domain}:{port} with {security}");
		let client = match security
		{
			IMAPSecurity::SSLTLS => imap::connect(addr, domain, &tls),
			IMAPSecurity::STARTTLS => imap::connect_starttls(addr, domain, &tls),
		}.expect(format!("Could not create IMAP client: {security}").as_str());
		let mut session = match &source.auth
		{
			IMAPAuthentication::PASSWORD(pass) => client.login(&source.username, pass).expect(format!("Could not log in with provided credentials: {0}@{source}", &source.username).as_str()),
			auth => panic!("Unknown auth method: {auth}"),
		};
		let mut message_count = 0;
		let mut mailbox_count = 0;
		let mailboxes = session.list(None, Some("*")).expect("Could not list mailboxes.");
		for mailbox_name in mailboxes.iter()
		{
			let mailbox_name = mailbox_name.name();
			if FOLDER_BLACKLIST.iter().any(|str| str.eq(&mailbox_name)) { continue; }
			let mailbox = session.select(mailbox_name);
			if mailbox.is_err()
			{
				log!(false, "Could not select mailbox {mailbox_name}: {}", mailbox.unwrap_err().to_string());
				continue;
			}
			let mailbox = mailbox.unwrap();
			let mailbox_message_count = &mailbox.exists;
			log!(false, "{mailbox_name}: x{mailbox_message_count} messages found.");
			if mailbox_message_count.eq(&0) { continue; }
			message_count += mailbox_message_count;
			mailbox_count += 1;
			let messages = session.fetch("1:*", "(BODY.PEEK[] UID FLAGS)").expect(format!("Could not fetch messages: {mailbox_name} {source}").as_str());
			for message in &messages
			{
				match process_message(&mut session, &ProcessMessageRequest
				{
					source: &source,
					message: &message,
					triggers: &triggers,
					tmp_dir: &tmp_dir,
					dry_run: dry_run,
					mailbox: &mailbox_name
				})
				{
					Err(err) =>
					{
						log!(true, "Processing message failed: {}", err)
					},
					_ => (),
				};
			}
		}
		if session.logout().is_err()
		{
			// Squashing this error due to known incompatibility.
			log!(false, "Error logging out of session: {source}");
		}
		log!(true, "Finished processing x{message_count} messages in {mailbox_count} mailboxes from {source}.");
	}
	std::fs::remove_dir_all(&tmp_dir).or_else(|err|
	{
		log!(true, "Failed to delete temp directory:{}", err);
		Err(err)
	}).ok();
}

fn process_message<T: Read + Write>(session: &mut imap::Session<T>, req: &ProcessMessageRequest) -> Result<(), BoxErr>
{
	let message = req.message;
	let dry_run = req.dry_run;
	let source = req.source;
	let tmp_dir = req.tmp_dir;
	let triggers = req.triggers;
	let mailbox = req.mailbox;
	let mut properties = HashMap::<String, String>::new();
	let message_num = message.message;
	let label = format!("{source}:{mailbox}:#{message_num}");
	let uid = message.uid.ok_or(format!("Message had no UID: {}", label).as_str())?;
	let label = format!("{source}:{uid}");
	let body_raw = message.body().ok_or(format!("Message had no body: {}", label).as_str())?;
	let message = mailparse::parse_mail(body_raw)?;
	let body = message.get_body()?;
	let headers = message.headers;
	let sender = headers.get_first_value("From").ok_or(format!("Message had no sender: {}", label).as_str())?;
	let label = format!("[{source}]{sender}");
	properties.insert("FROM".to_string(), sender);
	let date = headers.get_first_value("Date").ok_or(format!("Message had no date: {}", label).as_str())?;
	let label = format!("{label}@{date}");
	properties.insert("DATE".to_string(), date.clone());
	let subject = headers.get_first_value("Subject").ok_or(format!("Message had no subject: {}", label).as_str())?;
	let label = format!("{label}: {subject}");
	properties.insert("SUBJECT".to_string(), subject.clone());
	let body = body;
	properties.insert("BODY".to_string(), body);
	let attachments_dir = format!("{}/attachments", tmp_dir);
	std::fs::create_dir_all(&attachments_dir).expect(format!("Could not create attachments directory: {}", &attachments_dir).as_str());
	properties.insert("TMPDIR".to_string(), tmp_dir.to_string());
	properties.insert("ATTACHMENTS_DIR".to_string(), attachments_dir.clone());
	properties.insert("MAILBOX".to_string(), mailbox.to_string());
	let mut attachments = HashMap::<String, Vec<u8>>::new();
	for part in message.subparts
	{
		let disposition = part.get_content_disposition();
		if disposition.disposition == Attachment
		{
			let filename = disposition.params.get("filename")
				.ok_or(format!("Message had attachment with no filename: {}", label).as_str())?;
			let data = part.get_body_raw()?;
			attachments.insert(filename.clone(), data);
		}
	}
	let mut processed_message = false;
	let mut outbox = None;
	for trigger in triggers
	{
		if should_run_trigger(&trigger, &properties, &attachments)?
		{
			log!(true, "Processing: {}", label);
			let seq_set = format!("{}", uid);
			let run_result = run_trigger(trigger, &properties);
			if run_result.is_err()
			{
				session.uid_store(seq_set.as_str(), "-FLAGS (\\Seen)")?;
				session.uid_store(seq_set, "+FLAGS (\\Flagged)")?;
				return Err(run_result.unwrap_err());
			}
			let (trigger_outbox, mark_seen) = run_result.unwrap();
			if mark_seen.is_some() && !dry_run { session.uid_store(seq_set, format!("{}FLAGS (\\Seen)", if mark_seen.unwrap() {"+"} else {"-"}).as_str())?; }
			processed_message = true;
			outbox = trigger_outbox;
			break;
		}
	}
	std::fs::remove_dir_all(&attachments_dir).or_else(|err|
	{
		log!(true, "Failed to delete attachments directory: {}", err);
		Err(err)
	}).ok();
	match outbox
	{
		Some(outbox) =>
		{
			match dry_run
			{
				true => log!(true, "Keeping message due to no_delete flag: {}", label),
				false =>
				{
					log!(true, "Moving message to {outbox}: {label}");
					let seq_set = format!("{}", uid);
					session.uid_mv(seq_set, outbox).map_err(|err| err.to_string())?;
				}
			};
		},
		_ =>
		{
			if processed_message { println!("WARNING: Message was processed, but trigger did not assign to outbox: {label}"); }
		},
	};
	Ok(())
}

fn should_run_trigger(trigger: &JsonValue, properties: &HashMap<String, String>, attachments: &HashMap<String, Vec<u8>>) -> Result<bool,BoxErr>
{
	let inbox_json_value = JsonValue::String("INBOX".to_string());
	let default_mailbox_pair = ("MAILBOX", &inbox_json_value);
	let mut properties = properties.clone();
	let conditions = &trigger["conditions"];
	let mailbox_condition = &conditions["MAILBOX"];
	let conditions = conditions.entries().chain
	(
		std::iter::once(default_mailbox_pair).filter(|_| mailbox_condition.is_null())
	);
	for (key, condition) in conditions
	{
		let invert = key.starts_with("!");
		let key = key.strip_prefix("!").unwrap_or(key);
		let conditon_values = match condition.is_array()
		{
			true => condition.members().collect(),
			false => vec!(condition),
		};
		let mut matched_one = false;
		for val in conditon_values
		{
			let val_str = val.as_str().ok_or(format!("{} value must be a regex string or array of regex strings.", &key));
			let regex = match val_str
			{
				Ok(str) => Regex::new(&str).map_err(|e| BoxErr::from(e)),
				Err(e) => Err(BoxErr::from(e))
			};
			let val_num = val.as_i64().ok_or(format!("{} value must be a number.", &key));
			if match key
			{
				"attachment" =>
				{
					let attachments_dir = properties.get("ATTACHMENTS_DIR").unwrap();
					let mut ret_val = false;
					let mut file_paths = Vec::<String>::new();
					let regex = regex?;
					for (name, data) in attachments
					{
						if regex.is_match(name)?
						{
							let extension = Regex::new(r"\.[a-zA-Z]*$")?
								.captures(name)?
								.and_then(|cap| { cap.get(0) })
								.map(|mat| { mat.as_str() })
								.unwrap_or(".bin");
							ret_val = true;
							let file_path = format!("{}/munged_file_name.{}{}", attachments_dir, file_paths.len(), extension);
							std::fs::write(&file_path, data)?;
							file_paths.push(file_path);
						}
					}
					if ret_val
					{
						properties.insert("ATTACHMENTS".to_string(), file_paths.join(" "));
					}
					ret_val
				},
				"min_age" =>
				{
					let msg_date = properties.get("DATE").unwrap();
					let msg_date = DateTime::parse_from_rfc2822(msg_date)
						.expect(format!("Could not parse message date: {}", msg_date).as_str());
					let msg_date = msg_date.with_timezone(&chrono::Utc);
					let current_date = chrono::Utc::now();
					let age = current_date.signed_duration_since(msg_date);
					age >= Duration::days(val_num?)
				},
				key =>
				{
					let prop = properties.get(key).ok_or(format!("Cannot match nonexistant property: {}", &key).as_str())?;
					regex?.is_match(prop)?
				}
			}
			{
				matched_one = true;
				break;
			}
		}
		if matched_one == invert { return Ok(false); }
	};
	return Ok(true);
}

fn run_trigger(trigger: &JsonValue, properties: &HashMap<String, String>) -> Result<(Option<String>, Option<bool>), BoxErr>
{
	let exec = trigger["exec"].as_str().ok_or("exec property must be a string")?;
	let args_config: Vec<&JsonValue> = trigger["args"].members().collect();
	let mut args = Vec::<String>::new();
	for arg in args_config
	{
		let arg = get_string_value_with_interpolation(&arg, &properties)
			.ok_or("Failed to parse argument.")?;
		args.push(arg);
	}
	log!(true, "Executing: {} {}", exec, args.join(" "));
	let mut child;
	{
		let mut cmd = Command::new(exec);
		child = cmd
			.args(&args)
			.envs(properties)
			.env("BODY", "")
			.stdout(Stdio::inherit())
			.stderr(Stdio::inherit())
			.stdin(Stdio::piped())
			.spawn()?;
		let mut stdin = child.stdin.take().unwrap();
		stdin.write_all(properties.get("BODY").unwrap().as_bytes()).ok();
	}
	let status = child.wait()?;
	status.exit_ok()?;
	let outbox = trigger["outbox"].as_str().map(|s| s.to_string());
	let mark_unread = trigger["mark_unread"].as_bool().unwrap_or(false);
	let mark_read = trigger["mark_read"].as_bool().unwrap_or(!mark_unread);
	if mark_unread && mark_read {Err(BoxErr::from("Cannot specify both mark_read and mark_unread."))}
	else if mark_unread || mark_read {Ok((outbox, Some(mark_read)))}
	else {Ok((outbox, None))}
}